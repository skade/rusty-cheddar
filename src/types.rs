//! Helper functions for turning Rust types into string representations of C types.

use binder::compiler;
use binder::compiler::session;
use binder::compiler::syntax;
use binder::compiler::utils;

/// Turn a Rust type with an associated name or type into a C type.
///
/// `assoc` is required because of function pointers. Function pointers have to "wrap" something,
/// e.g.
///
/// ```c
/// float* (*my_func)(int a, char b);
/// ```
///
/// where the function pointer "wraps" the name `my_func`, or
///
/// ```c
/// void (*foo(int bar, void* baz))(char* bux);
/// ```
///
/// where the function pointer "wraps" the partial declaration
///
/// ```c
/// foo(int bar, void* baz)
/// ```
///
/// . `assoc` is therefore the thing which the function pointer "wraps".
pub fn rust_to_c(session: &mut session::Session, ty: &syntax::ast::Ty, assoc: &str, standard: super::Standard) -> Result<String, compiler::Stop> {
    match ty.node {
        // Function pointers make life an absolute pain here.
        syntax::ast::TyKind::BareFn(ref bare_fn) => fn_ptr_to_c(session, bare_fn, ty.span, assoc, standard),
        // All other types just have a name associated with them.
        _ => Ok(format!("{} {}", try!(anon_rust_to_c(session, ty, standard)), assoc)),
    }
}

/// Turn a Rust type into a C type.
fn anon_rust_to_c(session: &mut session::Session, ty: &syntax::ast::Ty, standard: super::Standard) -> Result<String, compiler::Stop> {
    match ty.node {
        // Function pointers should not be in this function.
        syntax::ast::TyKind::BareFn(..) => Err(session.span_err(
            ty.span,
            "C function pointers must have a name or function declaration associated with them",
        )),
        // Standard pointers.
        syntax::ast::TyKind::Ptr(ref ptr) => ptr_to_c(session, ptr, standard),
        // Plain old types.
        syntax::ast::TyKind::Path(None, ref path) => path_to_c(session, path),
        // Possibly void, likely not.
        _ => {
            let new_type = utils::ty_to_string(ty);
            if new_type == "()" {
                Ok("void".into())
            } else {
                Err(session.span_err(
                    ty.span,
                    &format!("cheddar can not handle the type `{}`", new_type),
                ))
            }
        },
    }
}

/// Turn a Rust pointer (*mut or *const) into the correct C form.
fn ptr_to_c(session: &mut session::Session, ty: &syntax::ast::MutTy, standard: super::Standard) -> Result<String, compiler::Stop> {
    let new_type = try!(anon_rust_to_c(session, &ty.ty, standard));
    let const_spec = match ty.mutbl {
        // *const T
        syntax::ast::Mutability::Immutable => " const",
        // *mut T
        syntax::ast::Mutability::Mutable => "",
    };

    Ok(format!("{}{}*", new_type, const_spec))
}

/// Turn a Rust function pointer into a C function pointer.
///
/// Rust function pointers are of the form
///
/// ```ignore
/// fn(arg1: Ty1, ...) -> RetTy
/// ```
///
/// C function pointers are of the form
///
/// ```C
/// RetTy (*inner)(Ty1 arg1, ...)
/// ```
///
/// where `inner` could either be a name or the rest of a function declaration.
fn fn_ptr_to_c(session: &mut session::Session, fn_ty: &syntax::ast::BareFnTy, fn_span: syntax::codemap::Span, inner: &str, standard: super::Standard) -> Result<String, compiler::Stop> {
    use binder::compiler::syntax::abi::Abi;
    match fn_ty.abi {
        // If it doesn't have a C ABI it can't be called from C.
        Abi::C | Abi::Cdecl | Abi::Stdcall | Abi::Fastcall | Abi::System => {},
        _ => return Err(compiler::Stop::Abort),
    }

    if !fn_ty.lifetimes.is_empty() {
        return Err(session.span_err(fn_span, "cheddar can not handle lifetimes"));
    }

    let fn_decl: &syntax::ast::FnDecl = &*fn_ty.decl;

    let mut buf_without_return = format!("(*{})(", inner);

    let has_args = !fn_decl.inputs.is_empty();

    for arg in &fn_decl.inputs {
        let arg_name = utils::pat_to_string(&*arg.pat);
        let arg_type = try!(rust_to_c(session, &*arg.ty, &arg_name, standard));
        buf_without_return.push_str(&arg_type);
        buf_without_return.push_str(", ");
    }

    if has_args {
        // Remove the trailing comma and space.
        buf_without_return.pop();
        buf_without_return.pop();

        buf_without_return.push(')');
    } else {
        buf_without_return.push_str("void)");
    }

    let output_type = &fn_decl.output;
    let full_declaration = match *output_type {
        syntax::ast::FunctionRetTy::None(..) => {
            if standard >= super::Standard::C11 {
                format!("noreturn void {}", buf_without_return)
            } else {
                format!("void {}", buf_without_return)
            }
        },
        syntax::ast::FunctionRetTy::Default(..) => format!("void {}", buf_without_return),
        syntax::ast::FunctionRetTy::Ty(ref ty) => try!(rust_to_c(session, &*ty, &buf_without_return, standard)),
    };


    Ok(full_declaration)
}

/// Convert a Rust path type (`my_mod::MyType`) to a C type.
///
/// Types hidden behind modules are almost certainly custom types (which wouldn't work) except
/// types in `libc` which we special case.
fn path_to_c(session: &session::Session, path: &syntax::ast::Path) -> Result<String, compiler::Stop> {
    // I don't think this is possible.
    if path.segments.is_empty() {
        Err(session.span_bug(path.span, "what the fuck have you done to this type?! (ノಠ益ಠ)ノ︵ ┻━┻"))
    // Types in modules, `my_mod::MyType`.
    } else if path.segments.len() > 1 {
        // Although we could use `syntex_syntax::print::pprust::path_to_string` here, we would be
        // prone to allowing things like `libc::some_module::SomeType` so we instead do it
        // manually.
        let (ty, module) = path.segments.split_last()
            .expect("already checked that there were at least two elements");
        let ty: &str = &ty.identifier.name.as_str();
        let mut segments = Vec::with_capacity(module.len());
        for segment in module {
            segments.push(String::from(&*segment.identifier.name.as_str()));
        }
        let module = segments.join("::");
        match &*module {
            "libc" => Ok(libc_ty_to_c(ty).into()),
            "std::os::raw" => Ok(os_raw_ty_to_c(ty).into()),
            _ => Err(session.span_err(
                path.span,
                "cheddar can not handle types in other modules (except `libc` and `std::os::raw`)",
            )),
        }
    } else {
        Ok(rust_ty_to_c(&path.segments[0].identifier.name.as_str()).into())
    }
}

/// Convert a Rust type from `libc` into a C type.
///
/// Most map straight over but some have to be converted.
fn libc_ty_to_c(ty: &str) -> &str {
    match ty {
        "c_void" => "void",
        "c_float" => "float",
        "c_double" => "double",
        "c_char" => "char",
        "c_schar" => "signed char",
        "c_uchar" => "unsigned char",
        "c_short" => "short",
        "c_ushort" => "unsigned short",
        "c_int" => "int",
        "c_uint" => "unsigned int",
        "c_long" => "long",
        "c_ulong" => "unsigned long",
        "c_longlong" => "long long",
        "c_ulonglong" => "unsigned long long",
        // All other types should map over to C.
        ty => ty,
    }
}

/// Convert a Rust type from `std::os::raw` into a C type.
///
/// These mostly mirror the libc crate.
fn os_raw_ty_to_c(ty: &str) -> &str {
    match ty {
        "c_void" => "void",
        "c_char" => "char",
        "c_double" => "double",
        "c_float" => "float",
        "c_int" => "int",
        "c_long" => "long",
        "c_longlong" => "long long",
        "c_schar" => "signed char",
        "c_short" => "short",
        "c_uchar" => "unsigned char",
        "c_uint" => "unsigned int",
        "c_ulong" => "unsigned long",
        "c_ulonglong" => "unsigned long long",
        "c_ushort" => "unsigned short",
        // All other types should map over to C.
        ty => ty,
    }
}

/// Convert any Rust type into C.
///
/// This includes user-defined types. We currently trust the user not to use types which we don't
/// know the structure of (like String).
fn rust_ty_to_c(ty: &str) -> &str {
    match ty {
        "()" => "void",
        "f32" => "float",
        "f64" => "double",
        "i8" => "int8_t",
        "i16" => "int16_t",
        "i32" => "int32_t",
        "i64" => "int64_t",
        "isize" => "intptr_t",
        "u8" => "uint8_t",
        "u16" => "uint16_t",
        "u32" => "uint32_t",
        "u64" => "uint64_t",
        "usize" => "uintptr_t",
        // This is why we write out structs and enums as `typedef ...`.
        // We `#include <stdbool.h>` so bool is handled.
        ty => ty,
    }
}


#[cfg(test)]
mod test {
    use binder::compiler::test::parse_ty_from_str as ty;

    #[test]
    #[ignore]
    fn generics() {
        let name = "azazael";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        let source = "Result<f64, i32>";
        let typ = super::anon_rust_to_c(&mut session, &ty(source), standard);
        assert_eq!(typ, Err(::binder::compiler::Stop::Abort));

        let source = "Option<i16>";
        let typ = super::rust_to_c(&mut session, &ty(source), name, standard);
        assert_eq!(typ, Err(::binder::compiler::Stop::Abort));
    }

    #[test]
    fn pure_rust_types() {
        let type_map = [
            ("()", "void"),
            ("f32", "float"),
            ("f64", "double"),
            ("i8", "int8_t"),
            ("i16", "int16_t"),
            ("i32", "int32_t"),
            ("i64", "int64_t"),
            ("isize", "intptr_t"),
            ("u8", "uint8_t"),
            ("u16", "uint16_t"),
            ("u32", "uint32_t"),
            ("u64", "uint64_t"),
            ("usize", "uintptr_t"),
        ];

        let name = "gabriel";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        for &(rust_type, correct_c_type) in &type_map {
            let parsed_c_type = super::anon_rust_to_c(&mut session, &ty(rust_type), standard)
                .expect(&format!("error while parsing {:?} with no name", rust_type));
            assert_eq!(parsed_c_type, correct_c_type);

            let parsed_c_type = super::rust_to_c(&mut session, &ty(rust_type), name, standard)
                .expect(&format!("error while parsing {:?} with name {:?}", rust_type, name));
            assert_eq!(parsed_c_type, format!("{} {}", correct_c_type, name));
        }
    }

    #[test]
    fn libc_types() {
        let type_map = [
            ("libc::c_void", "void"),
            ("libc::c_float", "float"),
            ("libc::c_double", "double"),
            ("libc::c_char", "char"),
            ("libc::c_schar", "signed char"),
            ("libc::c_uchar", "unsigned char"),
            ("libc::c_short", "short"),
            ("libc::c_ushort", "unsigned short"),
            ("libc::c_int", "int"),
            ("libc::c_uint", "unsigned int"),
            ("libc::c_long", "long"),
            ("libc::c_ulong", "unsigned long"),
            ("libc::c_longlong", "long long"),
            ("libc::c_ulonglong", "unsigned long long"),
            // Some other common ones.
            ("libc::size_t", "size_t"),
            ("libc::dirent", "dirent"),
            ("libc::FILE", "FILE"),
        ];

        let name = "lucifer";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        for &(rust_type, correct_c_type) in &type_map {
            let parsed_c_type = super::anon_rust_to_c(&mut session, &ty(rust_type), standard)
                .expect(&format!("error while parsing {:?} with no name", rust_type));
            assert_eq!(parsed_c_type, correct_c_type);

            let parsed_c_type = super::rust_to_c(&mut session, &ty(rust_type), name, standard)
                .expect(&format!("error while parsing {:?} with name {:?}", rust_type, name));
            assert_eq!(parsed_c_type, format!("{} {}", correct_c_type, name));
        }
    }

    #[test]
    fn os_raw_types() {
        let type_map = [
            ("std::os::raw::c_void", "void"),
            ("std::os::raw::c_float", "float"),
            ("std::os::raw::c_double", "double"),
            ("std::os::raw::c_char", "char"),
            ("std::os::raw::c_schar", "signed char"),
            ("std::os::raw::c_uchar", "unsigned char"),
            ("std::os::raw::c_short", "short"),
            ("std::os::raw::c_ushort", "unsigned short"),
            ("std::os::raw::c_int", "int"),
            ("std::os::raw::c_uint", "unsigned int"),
            ("std::os::raw::c_long", "long"),
            ("std::os::raw::c_ulong", "unsigned long"),
            ("std::os::raw::c_longlong", "long long"),
            ("std::os::raw::c_ulonglong", "unsigned long long"),
            // Some other common ones.
            ("std::os::raw::size_t", "size_t"),
            ("std::os::raw::dirent", "dirent"),
            ("std::os::raw::FILE", "FILE"),
        ];

        let name = "maalik";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        for &(rust_type, correct_c_type) in &type_map {
            let parsed_c_type = super::anon_rust_to_c(&mut session, &ty(rust_type), standard)
                .expect(&format!("error while parsing {:?} with no name", rust_type));
            assert_eq!(parsed_c_type, correct_c_type);

            let parsed_c_type = super::rust_to_c(&mut session, &ty(rust_type), name, standard)
                .expect(&format!("error while parsing {:?} with name {:?}", rust_type, name));
            assert_eq!(parsed_c_type, format!("{} {}", correct_c_type, name));
        }
    }

    #[test]
    fn const_pointers() {
        let name = "marut";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        let source = "*const u8";
        let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard)
            .expect(&format!("error while parsing {:?} with no name", source));
        assert_eq!(parsed_type, "uint8_t const*");

        let source = "*const ()";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("void const* {}", name));

        let source = "*const *const f64";
        let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard)
            .expect(&format!("error while parsing {:?} with no name", source));
        assert_eq!(parsed_type, "double const* const*");

        let source = "*const *const i64";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("int64_t const* const* {}", name));
    }

    #[test]
    fn mut_pointers() {
        let name = "raphael";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        let source = "*mut u16";
        let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard)
            .expect(&format!("error while parsing {:?} with no name", source));
        assert_eq!(parsed_type, "uint16_t*");

        let source = "*mut f32";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("float* {}", name));

        let source = "*mut *mut *mut i32";
        let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard)
            .expect(&format!("error while parsing {:?} with no name", source));
        assert_eq!(parsed_type, "int32_t***");

        let source = "*mut *mut i8";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("int8_t** {}", name));
    }

    #[test]
    fn mixed_pointers() {
        let name = "samael";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        let source = "*const *mut *const bool";
        let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard)
            .expect(&format!("error while parsing {:?} with no name", source));
        assert_eq!(parsed_type, "bool const** const*");

        let source = "*mut *mut *const libc::c_ulonglong";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("unsigned long long const*** {}", name));
    }

    #[test]
    fn function_pointers() {
        let name = "sariel";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        // TODO: these tests fail. why?
        // let source = "fn(a: bool)";
        // let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard);
        // assert_eq!(parsed_type, Err(::binder::compiler::Stop::Fail));

        // let source = "fn(a: i8) -> f64";
        // let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard);
        // assert_eq!(parsed_type, Err(::binder::compiler::Stop::Abort));

        let source = "extern fn(hi: libc::c_int) -> libc::c_double";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("double (*{})(int hi)", name));

        let source = "extern fn() -> libc::c_double";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("double (*{})(void)", name));
    }

    #[test]
    #[ignore]
    fn nullable_function_pointers() {
        let name = "seraphiel";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        let source = "Option<extern fn()>";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("void (*{})()", name));
    }

    #[test]
    fn paths() {
        let name = "zachariel";
        let mut session = ::binder::compiler::session::Session::default();
        let standard = ::Standard::C99;

        let source = "MyType";
        let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard)
            .expect(&format!("error while parsing {:?} with no name", source));
        assert_eq!(parsed_type, "MyType");

        let source = "SomeType";
        let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard)
            .expect(&format!("error while parsing {:?} with name {:?}", source, name));
        assert_eq!(parsed_type, format!("SomeType {}", name));

        // TODO: these tests fail. why?
        // let source = "my_mod::MyType";
        // let parsed_type = super::anon_rust_to_c(&mut session, &ty(source), standard);
        // assert_eq!(parsed_type, Err(::binder::compiler::Stop::Fail));

        // let source = "some_mod::SomeType";
        // let parsed_type = super::rust_to_c(&mut session, &ty(source), name, standard);
        // assert_eq!(parsed_type, Err(::binder::compiler::Stop::Fail));
    }
}
