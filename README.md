# rusty-cheddar

[![Build Status](https://gitlab.com/rusty-binder/rusty-cheddar/badges/master/build.svg)](https://gitlab.com/rusty-binder/rusty-cheddar/builds)
[![crates.io](http://meritbadge.herokuapp.com/rusty-cheddar)](https://crates.io/crates/rusty-cheddar)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](http://mit-license.org/)

**This project is part of a major rewrite of
[rusty-cheddar](https://github.com/Sean1708/rusty-cheddar) with an aim to make a more
language-agnostic bindings generator. Until rusty-binder is put on `crates.io` it should be
considered entirely work-in-progress and unusable. Thanks for your patience!**

Rusty-cheddar is a crate for converting Rust source files into C header files.

**A note on versioning:** While rusty-cheddar is still in a significant flux (i.e.
pre-`v1.0.0`) it will likely go through numerous breaking changes. However, until `v1.0.0`, any
time a breaking change is made the minor version will be bumped and any time a new feature is
added the path version will be bumped.

Rusty-cheddar examines your Rust source code and produces a header file of the bits which are C
compatible. See the documentation for `Generator` to get a better understanding of how this is
done.

The most useful way to use rusty-cheddar is in a build script. This can either be done using
the `run_build` convenience method provided in rusty-cheddar, or by using the more general
[rusty-binder] framework.

## rusty-cheddar

If the only bindings you want are a C header file then it makes sense to just rely on
[rusty-cheddar]. First you should set up the `Generator` type with your required settings, then
just call the `run_build` method to create the files.

```toml
# Cargo.toml

[package]
build = "build.rs"

[build-dependencies]
rusty-cheddar = "..."

[lib]
crate-type = ["dylib"]
```

```rust
// build.rs

extern crate cheddar;

fn main() {
    cheddar::Header::c99()
        .name("my_awesome_header.h")
        .insert_code("// This header is automatically generated - DO NOT CHANGE IT MANUALLY!")
        .run_build("target/include", Some("c_api"));
}
```

## rusty-binder

If you want to compile bindings for multiple languages, then you must hook into the more
general [rusty-binder] framework.

```toml
# Cargo.toml

[package]
build = "build.rs"

[build-dependencies]
rusty-binder = "..."
rusty-cheddar = "..."

[lib]
crate-type = ["dylib"]
```

```rust
// build.rs

extern crate binder;
extern crate cheddar;

fn main() {
     let mut c99 = cheddar::Header::c99();
     c99.name("my_header_c99.h");

     let mut c89 = cheddar::Header::with_standard(cheddar::Standard::C89);
     c89.name("my_header_c89.h");

     binder::Binder::new().expect("could not read cargo manifest")
         .module("capi")
         .output_directory("target/include")
         .register(c99)
         .register(c89)
         .run_build();
}
```


[rusty-binder]: https://gitlab.com/rusty-binder/rusty-binder
[rusty-cheddar]: https://gitlab.com/rusty-binder/rusty-cheddar

## Contributing

Contributions to rusty-cheddar are more than welcome.

### Bugs

If you find a bug or have a feature request please open an issue. I can't guarantee that I'll fix it
but I'll give it a damn good go.

If you find the source code unclear in any way then I consider that a bug. I try to make my source
code as clear as possible but I'm not very good at it, so any help in that regard is appreciated.

### PRs

I love pull requests, they tend to make my job much easier, so if you want to fix a bug or implement
a feature yourself then that would be great. If you're confused by anything or need some pointers on
how to proceed then feel free to open an issue so that I can help, otherwise [these docs] are a good
place to start.

This project uses [clog] so please make sure your commit messages adhere to [conventional format],
thanks!

[these docs]: http://manishearth.github.io/rust-internals-docs/syntax/ast/index.html
[clog]: https://github.com/clog-tool/clog-cli
[conventional format]: https://github.com/ajoslin/conventional-changelog/blob/a5505865ff3dd710cf757f50530e73ef0ca641da/conventions/angular.md
